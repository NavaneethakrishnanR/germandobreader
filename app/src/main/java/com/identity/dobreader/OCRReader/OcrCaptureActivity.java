package com.identity.dobreader.OCRReader;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.PixelCopy;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;
import com.google.android.material.snackbar.Snackbar;
import com.identity.dobreader.R;
import com.identity.dobreader.Tensor.Classifier;
import com.identity.dobreader.Tensor.TensorFlowImageClassifier;


import java.io.ByteArrayOutputStream;
import java.io.File;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static android.os.Environment.DIRECTORY_DOWNLOADS;

/**
 * Activity for the multi-tracker app.  This app detects text and displays the value with the
 * rear facing camera. During detection overlay graphics are drawn to indicate the position,
 * size, and contents of each TextBlock.
 */
public final class OcrCaptureActivity extends AppCompatActivity {
    private static final String TAG = "OcrCaptureActivity";

    // Intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;

    // Permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    // Constants used to pass extra data in the intent
    public static final String AutoFocus = "AutoFocus";
    public static final String UseFlash = "UseFlash";
    public static final String TextBlockObject = "String";
    public static String TextBlocks = "";

    private CameraSource mCameraSource;
    private CameraSourcePreview mPreview;
    private GraphicOverlay<OcrGraphic> mGraphicOverlay;

    public static String imgBitmap;

    // Helper objects for detecting taps and pinches.
    private ScaleGestureDetector scaleGestureDetector;
    private GestureDetector gestureDetector;

    private static final String MODEL_PATH = "german_model_no_quan_224_no_pre.tflite";
    private static final String LABEL_PATH = "labels.txt";
    private static final int INPUT_SIZE = 224;

    private Classifier classifier;

    private Executor executor = Executors.newSingleThreadExecutor();
    private TextView textViewResult,textViewResult1;
    private Button btnDetectObject, btnToggleCamera;

    /*private CameraView cameraView;*/
    ProgressDialog progressDialog;

    private FrameLayout frameCamera;
    private Camera camera;
    private CameraPreview cameraPreview;
    private int cameraType=0;

    /**
     * Initializes the UI and creates the detector pipeline.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.ocr_capture);


        mPreview = findViewById(R.id.preview);
        mGraphicOverlay = findViewById(R.id.graphicOverlay);

        progressDialog = new ProgressDialog(OcrCaptureActivity.this);
        progressDialog.setMessage("Processing...");
        frameCamera = (FrameLayout) findViewById(R.id.frameCamera);

        textViewResult = findViewById(R.id.textViewResult);
        textViewResult1 = findViewById(R.id.textViewResult1);
        textViewResult.setMovementMethod(new ScrollingMovementMethod());
        /*if (checkCameraHardware()) {

            camera=getCameraInstance(cameraType);
            cameraPreview=new CameraPreview(this,camera,cameraType);
            frameCamera.addView(cameraPreview);
            *//*setFocus();*//*
        } else {
            Toast.makeText(getApplicationContext(), "Device not support camera feature", Toast.LENGTH_SHORT).show();
        }*/

        /*cameraView.addCameraKitListener(new CameraKitEventListener() {
            @Override
            public void onEvent(CameraKitEvent cameraKitEvent) {

            }

            @Override
            public void onError(CameraKitError cameraKitError) {

            }

            @Override
            public void onImage(CameraKitImage cameraKitImage) {

                Bitmap bitmap = cameraKitImage.getBitmap();

                bitmap = Bitmap.createScaledBitmap(bitmap, INPUT_SIZE, INPUT_SIZE, false);

                final List<Classifier.Recognition> results = classifier.recognizeImage(bitmap);
                if(results.size()==1)
                {
                    if(results.get(0).getId().equals("1") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>60.0)
                        textViewResult.setText(results.get(0).getTitle());
                    else if(results.get(0).getId().equals("2") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>20.0) {
                        textViewResult.setText("GerMan ID");
                    }else if(results.get(0).getId().equals("3") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>20.0) {
                        textViewResult.setText("GerMan ID");
                    }else{
                        textViewResult.setText(results.get(0).getTitle());
                    }
                }else if(results.size()==2)
                {
                    if(results.get(0).getId().equals("1") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>50.0 )
                        textViewResult.setText("UnKnown");
                    else if(results.get(1).getId().equals("1") && Double.parseDouble(String.format("%.1f", results.get(1).getConfidence() * 100.0f ))>50.0 )
                        textViewResult.setText("UnKnown");
                    else if(results.get(0).getId().equals("0") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>20.0)
                        textViewResult.setText("GerMan ID");
                    else if(results.get(1).getId().equals("0") && Double.parseDouble(String.format("%.1f", results.get(1).getConfidence() * 100.0f ))>20.0)
                        textViewResult.setText("GerMan ID");
                    else if(results.get(0).getId().equals("2") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>20.0)
                        textViewResult.setText("GerMan ID");
                    else if(results.get(1).getId().equals("2") && Double.parseDouble(String.format("%.1f", results.get(1).getConfidence() * 100.0f ))>20.0)
                        textViewResult.setText("GerMan ID");
                    else
                        textViewResult.setText("UnKnown");

                }else if(results.size()==3)
                {
                    if(results.get(0).getId().equals("1") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>50.0 )
                        textViewResult.setText("UnKnown");
                    else if(results.get(1).getId().equals("1") && Double.parseDouble(String.format("%.1f", results.get(1).getConfidence() * 100.0f ))>50.0 )
                        textViewResult.setText("UnKnown");
                    else if(results.get(2).getId().equals("1") && Double.parseDouble(String.format("%.1f", results.get(2).getConfidence() * 100.0f ))>50.0 )
                        textViewResult.setText("UnKnown");
                    else if(results.get(0).getId().equals("0") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>20.0 )
                        textViewResult.setText("GerMan ID");
                    else if(results.get(1).getId().equals("0") && Double.parseDouble(String.format("%.1f", results.get(1).getConfidence() * 100.0f ))>20.0 )
                        textViewResult.setText("GerMan ID");
                    else if(results.get(2).getId().equals("0") && Double.parseDouble(String.format("%.1f", results.get(2).getConfidence() * 100.0f ))>20.0 )
                        textViewResult.setText("GerMan ID");
                    else if(results.get(0).getId().equals("2") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>20.0 )
                        textViewResult.setText("GerMan ID");
                    else if(results.get(1).getId().equals("2") && Double.parseDouble(String.format("%.1f", results.get(1).getConfidence() * 100.0f ))>20.0 )
                        textViewResult.setText("GerMan ID");
                    else if(results.get(2).getId().equals("2") && Double.parseDouble(String.format("%.1f", results.get(2).getConfidence() * 100.0f ))>20.0 )
                        textViewResult.setText("GerMan ID");
                    else
                        textViewResult.setText("UnKnown");

                }

                Log.e("Result",textViewResult.getText().toString());
                if(progressDialog.isShowing())
                    progressDialog.dismiss();
                 Intent data = new Intent();
                data.putExtra(TextBlockObject, TextBlocks);
                data.putExtra("cardType", textViewResult.getText().toString());


                setResult(CommonStatusCodes.SUCCESS, data);
                finish();
            }

            @Override
            public void onVideo(CameraKitVideo cameraKitVideo) {

            }
        });
*/


        // read parameters from the intent used to launch the activity.
        boolean autoFocus = getIntent().getBooleanExtra(AutoFocus, false);
        boolean useFlash = getIntent().getBooleanExtra(UseFlash, false);

        // Check for the camera permission before accessing the camera.  If the
        // permission is not granted yet, request permission.
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED) {
            createCameraSource(autoFocus, useFlash);
        } else {
            requestCameraPermission();
        }

        gestureDetector = new GestureDetector(this, new CaptureGestureListener());
        scaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());

        /*Snackbar.make(mGraphicOverlay, "Tap to capture. Pinch/Stretch to zoom",
                Snackbar.LENGTH_LONG)
                .show();*/
    }

    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void initTensorFlowAndLoadModel() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    classifier = TensorFlowImageClassifier.create(
                            getAssets(),
                            MODEL_PATH,
                            LABEL_PATH,
                            INPUT_SIZE);

                } catch (final Exception e) {
                    throw new RuntimeException("Error initializing TensorFlow!", e);
                }
            }
        });
    }
    private void requestCameraPermission() {
        Log.w(TAG, "Camera permission is not granted. Requesting permission");

        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(mGraphicOverlay, R.string.permission_camera_rationale,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.ok, listener)
                .show();
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        boolean b = scaleGestureDetector.onTouchEvent(e);

        boolean c = gestureDetector.onTouchEvent(e);

        return b || c || super.onTouchEvent(e);
    }

    /**
     * Creates and starts the camera.  Note that this uses a higher resolution in comparison
     * to other detection examples to enable the ocr detector to detect small text samples
     * at long distances.
     * <p>
     * Suppressing InlinedApi since there is a check that the minimum version is met before using
     * the constant.
     */
    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        Context context = getApplicationContext();

        // A text recognizer is created to find text.  An associated processor instance
        // is set to receive the text recognition results and display graphics for each text block
        // on screen.
        TextRecognizer textRecognizer = new TextRecognizer.Builder(context).build();
        textRecognizer.setProcessor(new OcrDetectorProcessor(mGraphicOverlay));

        if (!textRecognizer.isOperational()) {
            // Note: The first time that an app using a Vision API is installed on a
            // device, GMS will download a native libraries to the device in order to do detection.
            // Usually this completes before the app is run for the first time.  But if that
            // download has not yet completed, then the above call will not detect any text,
            // barcodes, or faces.
            //
            // isOperational() can be used to check if the required native libraries are currently
            // available.  The detectors will automatically become operational once the library
            // downloads complete on device.
            Log.w(TAG, "Detector dependencies are not yet available.");

            // Check for low storage.  If there is low storage, the native library will not be
            // downloaded, so detection will not become operational.
            IntentFilter lowstorageFilter = new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW);
            boolean hasLowStorage = registerReceiver(null, lowstorageFilter) != null;

            if (hasLowStorage) {
                Toast.makeText(this, R.string.low_storage_error, Toast.LENGTH_LONG).show();
                Log.w(TAG, getString(R.string.low_storage_error));
            }
        }

        // Creates and starts the camera.  Note that this uses a higher resolution in comparison
        // to other detection examples to enable the text recognizer to detect small pieces of text.
        mCameraSource =
                new CameraSource.Builder(getApplicationContext(), textRecognizer)
                        .setFacing(CameraSource.CAMERA_FACING_BACK)
                        .setRequestedPreviewSize(1280, 1024)
                        .setRequestedFps(2.0f)
                        .setFlashMode(useFlash ? Camera.Parameters.FLASH_MODE_TORCH : null)
                        .setFocusMode(autoFocus ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE : null)
                        .build();
    }

    /**
     * Restarts the camera.
     */
    @Override
    protected void onResume() {
        super.onResume();

        startCameraSource();
    }

    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        if (mPreview != null) {
            mPreview.stop();
        }
        if(frameCamera.getVisibility() == View.VISIBLE && camera!= null)
            camera.stopPreview();
    }

    /**
     * Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPreview != null) {
            mPreview.release();
        }
        if(frameCamera.getVisibility() == View.VISIBLE) {
            executor.execute(new Runnable() {
                @Override
                public void run() {
                    classifier.close();
                }
            });
        }
    }

    /**
     * Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <p>
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     * </p>
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int)
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Camera permission granted - initialize the camera source");
            // We have permission, so create the camerasource
            boolean autoFocus = getIntent().getBooleanExtra(AutoFocus, false);
            boolean useFlash = getIntent().getBooleanExtra(UseFlash, false);
            createCameraSource(autoFocus, useFlash);
            return;
        }

        Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Multitracker sample")
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }

    /**
     * Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created.
     */
    private void startCameraSource() throws SecurityException {
        // Check that the device has play services available.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(
                getApplicationContext());
        if (code != ConnectionResult.SUCCESS) {
            Dialog dlg =
                    GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS);
            dlg.show();
        }

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource, mGraphicOverlay);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.", e);
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    /**
     * onTap is called to capture the first TextBlock under the tap location and return it to
     * the Initializing Activity.
     *
     * @param rawX - the raw position of the tap
     * @param rawY - the raw position of the tap.
     * @return true if the activity is ending.
     */
    private boolean onTap(float rawX, float rawY) {
        Log.e(TAG, "onTap"+", called");
        OcrGraphic graphic = mGraphicOverlay.getGraphicAtLocation(rawX, rawY);
        TextBlock text = null;
        if (graphic != null) {
            text = graphic.getTextBlock();
            if (text != null && text.getValue() != null) {
                Intent data = new Intent();
                TextBlocks = text.getValue();
                data.putExtra(TextBlockObject, text.getValue());
                setResult(CommonStatusCodes.SUCCESS, data);
                finish();
            } else {
                Log.d(TAG, "text data is null");
            }
        } else {
            Log.d(TAG, "no text detected");
        }
        return text != null;
    }


    public void selectAll(View view) {
        /*if(!progressDialog.isShowing())
            progressDialog.show();*/
        Set<OcrGraphic> graphic = mGraphicOverlay.getGraphic();
        TextBlock text;
        if (graphic != null && graphic.size() > 0) {
            OcrGraphic[] myGraphic = graphic.toArray(new OcrGraphic[graphic.size()]);
            Arrays.sort(myGraphic);
            StringBuilder mText = new StringBuilder();
            for (OcrGraphic mGraphic : myGraphic) {
                text = mGraphic.getTextBlock();
                if (text != null && text.getValue() != null) {
                    if (mText.toString().equals("")) {
                        mText.append(text.getValue());
                    } else {
                        mText.append("\n");
                        mText.append(text.getValue());
                    }
                } else {
                    Log.d(TAG, "text data is null");
                }
            }
            if (mText.length() != 0) {
                TextBlocks = mText.toString();
                frameCamera.setVisibility(View.VISIBLE);
                camera=getCameraInstance(cameraType);
                cameraPreview=new CameraPreview(this,camera,cameraType);
                frameCamera.addView(cameraPreview);
                setFocus();
                initTensorFlowAndLoadModel();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        camera.takePicture(myShutterCallback,null,  pictureCallback);
                    }
                },100);

                /*Intent data = new Intent();
                data.putExtra(TextBlockObject, TextBlocks);
                data.putExtra("cardType", textViewResult1.getText().toString());

                setResult(CommonStatusCodes.SUCCESS, data);
                finish();*/
            }
        } else {
            Log.d(TAG, "no text detected");
        }


    }

    private class CaptureGestureListener extends GestureDetector.SimpleOnGestureListener {

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            return onTap(e.getRawX(), e.getRawY()) || super.onSingleTapConfirmed(e);
        }
    }

    private class ScaleListener implements ScaleGestureDetector.OnScaleGestureListener {

        /**
         * Responds to scaling events for a gesture in progress.
         * Reported by pointer motion.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         * @return Whether or not the detector should consider this event
         * as handled. If an event was not handled, the detector
         * will continue to accumulate movement until an event is
         * handled. This can be useful if an application, for example,
         * only wants to update scaling factors if the change is
         * greater than 0.01.
         */
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            return false;
        }

        /**
         * Responds to the beginning of a scaling gesture. Reported by
         * new pointers going down.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         * @return Whether or not the detector should continue recognizing
         * this gesture. For example, if a gesture is beginning
         * with a focal point outside of a region where it makes
         * sense, onScaleBegin() may return false to ignore the
         * rest of the gesture.
         */
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            return true;
        }

        /**
         * Responds to the end of a scale gesture. Reported by existing
         * pointers going up.
         * <p/>
         * Once a scale has ended, {@link ScaleGestureDetector#getFocusX()}
         * and {@link ScaleGestureDetector#getFocusY()} will return focal point
         * of the pointers remaining on the screen.
         *
         * @param detector The detector reporting the event - use this to
         *                 retrieve extended info about event state.
         */
        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            mCameraSource.doZoom(detector.getScaleFactor());
        }
    }



    private void scanGallery(Context cntx, String path) {
        try {
            MediaScannerConnection.scanFile(cntx, new String[] { path },null, new MediaScannerConnection.OnScanCompletedListener() {
                public void onScanCompleted(String path, Uri uri) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private String getString(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private boolean checkCameraHardware() {

        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            return true;
        } else {
            return false;
        }
    }
    private Camera getCameraInstance(int cameraType) {
        Camera camera = null;
//        Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
//        for (int i=0;i<Camera.getNumberOfCameras();i++){
//            Camera.CameraInfo camInfo = new Camera.CameraInfo();
//            Camera.getCameraInfo(i, camInfo);
//
//            if (camInfo.facing==(Camera.CameraInfo.CAMERA_FACING_FRONT)) {
        try {
            camera = Camera.open(cameraType);
        } catch (Exception e) {
            Log.d("tag","Error setting camera not open "+ e);
        }
//            }
//        }

        return camera;
    }

    public void setFocus(){
        Camera.Parameters params = camera.getParameters();
        if(params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)){
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        } else {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }

        camera.setParameters(params);
    }
    Camera.ShutterCallback myShutterCallback = new Camera.ShutterCallback(){

        @Override
        public void onShutter() {
            // TODO Auto-generated method stub

        }};

    private Camera.PictureCallback pictureCallback=new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File file= new File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS)+"/"+System.currentTimeMillis()+".jpg");
            if (file == null){
                Log.d("tag", "Error creating media file, check storage permissions: ");
                return;
            }

//            int width=getResources().getDisplayMetrics().widthPixels;
//            int hight=getResources().getDisplayMetrics().heightPixels;

            Bitmap bm= BitmapFactory.decodeByteArray(data,0, (data)!=null ? data.length :0);

            if(getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
//                Bitmap scaledBm=Bitmap.createScaledBitmap(bm,width,hight,true);
//                int w=scaledBm.getWidth();
//                int h=scaledBm.getHeight();

                Matrix matrix=new Matrix();
                matrix.setRotate(90);
                bm = Bitmap.createBitmap(bm, 0, 0, bm.getWidth(), bm.getHeight(), matrix, false);

            }
            bm = Bitmap.createScaledBitmap(bm, INPUT_SIZE, INPUT_SIZE, false);

            final List<Classifier.Recognition> results = classifier.recognizeImage(bm);
            if(results.size()==1)
            {
                if(results.get(0).getId().equals("1") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>60.0)
                    textViewResult.setText(results.get(0).getTitle());
                else if(results.get(0).getId().equals("2") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>20.0) {
                    textViewResult.setText("GerMan ID");
                }else if(results.get(0).getId().equals("3") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>20.0) {
                    textViewResult.setText("GerMan ID");
                }else{
                    textViewResult.setText(results.get(0).getTitle());
                }
            }else if(results.size()==2)
            {
                if(results.get(0).getId().equals("1") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>50.0 )
                    textViewResult.setText("UnKnown");
                else if(results.get(1).getId().equals("1") && Double.parseDouble(String.format("%.1f", results.get(1).getConfidence() * 100.0f ))>50.0 )
                    textViewResult.setText("UnKnown");
                else if(results.get(0).getId().equals("0") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>20.0)
                    textViewResult.setText("GerMan ID");
                else if(results.get(1).getId().equals("0") && Double.parseDouble(String.format("%.1f", results.get(1).getConfidence() * 100.0f ))>20.0)
                    textViewResult.setText("GerMan ID");
                else if(results.get(0).getId().equals("2") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>20.0)
                    textViewResult.setText("GerMan ID");
                else if(results.get(1).getId().equals("2") && Double.parseDouble(String.format("%.1f", results.get(1).getConfidence() * 100.0f ))>20.0)
                    textViewResult.setText("GerMan ID");
                else
                    textViewResult.setText("UnKnown");

            }else if(results.size()==3)
            {
                if(results.get(0).getId().equals("1") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>50.0 )
                    textViewResult.setText("UnKnown");
                else if(results.get(1).getId().equals("1") && Double.parseDouble(String.format("%.1f", results.get(1).getConfidence() * 100.0f ))>50.0 )
                    textViewResult.setText("UnKnown");
                else if(results.get(2).getId().equals("1") && Double.parseDouble(String.format("%.1f", results.get(2).getConfidence() * 100.0f ))>50.0 )
                    textViewResult.setText("UnKnown");
                else if(results.get(0).getId().equals("0") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>20.0 )
                    textViewResult.setText("GerMan ID");
                else if(results.get(1).getId().equals("0") && Double.parseDouble(String.format("%.1f", results.get(1).getConfidence() * 100.0f ))>20.0 )
                    textViewResult.setText("GerMan ID");
                else if(results.get(2).getId().equals("0") && Double.parseDouble(String.format("%.1f", results.get(2).getConfidence() * 100.0f ))>20.0 )
                    textViewResult.setText("GerMan ID");
                else if(results.get(0).getId().equals("2") && Double.parseDouble(String.format("%.1f", results.get(0).getConfidence() * 100.0f ))>20.0 )
                    textViewResult.setText("GerMan ID");
                else if(results.get(1).getId().equals("2") && Double.parseDouble(String.format("%.1f", results.get(1).getConfidence() * 100.0f ))>20.0 )
                    textViewResult.setText("GerMan ID");
                else if(results.get(2).getId().equals("2") && Double.parseDouble(String.format("%.1f", results.get(2).getConfidence() * 100.0f ))>20.0 )
                    textViewResult.setText("GerMan ID");
                else
                    textViewResult.setText("UnKnown");

            }

            Log.e("Result",textViewResult.getText().toString());
            if(progressDialog.isShowing())
                progressDialog.dismiss();
            Intent data1 = new Intent();
            data1.putExtra(TextBlockObject, TextBlocks);
            data1.putExtra("cardType", textViewResult.getText().toString());


            setResult(CommonStatusCodes.SUCCESS, data1);
            finish();
           /* try {
                FileOutputStream fileOutputStream=new FileOutputStream(file);
                bm.compress(Bitmap.CompressFormat.JPEG,100,fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }*/
            camera.startPreview();
        }
    };
}