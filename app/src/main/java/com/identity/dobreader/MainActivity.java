package com.identity.dobreader;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.identity.dobreader.OCRReader.OCRCapture;
import com.identity.dobreader.OCRReader.OcrCaptureActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.identity.dobreader.OCRReader.OcrCaptureActivity.TextBlockObject;

public class MainActivity extends AppCompatActivity {
    private TextView textView;
    private Button btn;
    public static ImageView img;
    private final int CAMERA_SCAN_TEXT = 0;
    private final int LOAD_IMAGE_RESULTS = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        img = findViewById(R.id.img);
        textView = findViewById(R.id.textView);
        btn = findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn.setEnabled(false);
                OCRCapture.Builder(MainActivity.this)
                        .setUseFlash(false)
                        .setAutoFocus(true)
                        .buildWithRequestCode(CAMERA_SCAN_TEXT);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        btn.setEnabled(true);
        if (data != null) {
            if (requestCode == CAMERA_SCAN_TEXT) {

                if (resultCode == CommonStatusCodes.SUCCESS) {

                    Log.e("Text",data.getStringExtra(TextBlockObject));
                   // if((data.getStringExtra("cardType") != null && !data.getStringExtra("cardType").equals("UnKnown")) && (data.getStringExtra(TextBlockObject).contains("IDD") ||data.getStringExtra(TextBlockObject).contains("TPD") ||data.getStringExtra(TextBlockObject).contains("IDp") || data.getStringExtra(TextBlockObject).contains("1DD") || data.getStringExtra(TextBlockObject).contains("1Dp")|| data.getStringExtra(TextBlockObject).contains("1op")|| data.getStringExtra(TextBlockObject).contains("1oD"))) {

                        if(getDOBs(data.getStringExtra(TextBlockObject)).size()>0)
                        {
                            textView.setText(getDOBs(data.getStringExtra(TextBlockObject)).get(0).substring(0,getDOBs(data.getStringExtra(TextBlockObject)).get(0).length()-1));
                        }else
                            textView.setText(getString(R.string.invalidCard));

                    //}else
                       // textView.setText(getString(R.string.invalidCard));


                }
            } else if (requestCode == LOAD_IMAGE_RESULTS) {
                Uri pickedImage = data.getData();
                String text = OCRCapture.Builder(this).getTextFromUri(pickedImage);
                if(data.getStringExtra(TextBlockObject).contains("IDD")) {
                    textView.setText(getDOB(data.getStringExtra(TextBlockObject)));
                }else
                    textView.setText("Invalid Card");
            }
        }
    }

    private static String getDate(String desc) {
        int count=0;
        String[] allMatches = new String[2];
        String strDate = "";
        Matcher m = Pattern.compile("(0[1-9]|[12][0-9]|3[01])[- ..](0[1-9]|1[012])[- ..]([0123456789][0-9])").matcher(desc);
        while (m.find()) {
            allMatches[count] = m.group();
            strDate = m.group();
            count++;
        }
        return strDate;
    }
    private static String getDOB(String desc) {
        String strDate = "";
        Matcher m = Pattern.compile("([0-9][0-9][0-9][0-9][0-9][0-9][0-9])").matcher(desc);
        while (m.find()) {
            strDate = m.group();
        }
        return strDate;
    }
    private static List<String> getDOBs(String desc) {
        List<String> data = new ArrayList<>();

        Matcher m = Pattern.compile("([0-9][0-9][0-9][0-9][0-9][0-9][0-9])").matcher(desc);
        while (m.find()) {
            data.add(m.group());
        }
        return data;
    }

}